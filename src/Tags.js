export const Tags = [
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c2bb1c68101b201481fe',
    name: 'First name',
    type: 'firstname',
    tag_type: 'task',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c2d01c68101b201481ff',
    name: 'Last Name',
    type: 'lastname',
    tag_type: 'task',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c3491c68101b20148200',
    name: 'Task Name',
    type: 'task_name',
    tag_type: 'task',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c3571c68101b20148201',
    name: 'Reminder Date',
    type: 'reminderdate',
    tag_type: 'task',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c37a1c68101b20148202',
    name: 'First Name',
    type: 'firstname',
    tag_type: 'paybill',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c3861c68101b20148203',
    name: 'Last Name',
    type: 'lastname',
    tag_type: 'paybill',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c3971c68101b20148204',
    name: 'Paybill Name',
    type: 'paybill_name',
    tag_type: 'paybill',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  },
  {
    deleted: 0,
    status: 1,
    created: '2021-03-10T12:52:46.025Z',
    modified: '2021-03-10T12:52:46.025Z',
    _id: '6048c3a81c68101b20148205',
    name: 'Reminder Date',
    type: 'reminderdate',
    tag_type: 'paybill',
    adminId: '5f30d618af4f104efab8d100',
    __v: 0
  }
]