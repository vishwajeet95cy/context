import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// import ReactDOM from "react-dom";
// import React, { useState } from "react";

// import authContext from "./authContext";
// import Login from "./Login";

// const App = () => {
//   const [authenticated, setAuthenticated] = useState(false);

//   return (
//     <authContext.Provider value={{ authenticated, setAuthenticated }}>
//       <div> user is {`${authenticated ? "" : "not"} authenticated`} </div>
//       <Login />
//     </authContext.Provider>
//   );
// };

// ReactDOM.render(<App />, document.getElementById("root"));


// import React from "react";
// import ReactDOM from "react-dom";
// import ContentEditable from "react-contenteditable";
// import sanitizeHtml from "sanitize-html";
// import "./styles.css";
// import { Tags } from "./Tags";

// class MyComponent extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       html: `<p>Hello <b>World</b> !</p><p>Paragraph 2</p>`,
//       editable: true
//     };
//   }
//   handleChange = (evt) => {
//     this.setState({ html: evt.target.value });
//   };

//   sanitizeConf = {
//     allowedTags: ["b", "i", "em", "strong", "a", "p", "h1", "span"],
//     allowedAttributes: { a: ["href"], span: ["class", "id", "contenteditable"] }
//   };

//   sanitize = () => {
//     this.setState({ html: sanitizeHtml(this.state.html, this.sanitizeConf) });
//   };

//   toggleEditable = () => {
//     this.setState({ editable: !this.state.editable });
//   };

//   handleAdds = (e, data) => {
//     e.preventDefault();
//     if (e.target.id === data.type) {
//       const content = document.createElement("span");
//       content.setAttribute('class', 'span-tag');
//       content.setAttribute('id', data._id);
//       content.setAttribute('contenteditable', 'false');
//       content.innerText = data.type

//       document.getElementById('vishwa').append(content)
//     }
//   }

//   render = () => {
//     return (
//       <div>
//         <h3>editable contents</h3>
//         <ContentEditable
//           className="editable"
//           tagName="pre"
//           id="vishwa"
//           html={this.state.html} // innerHTML of the editable div
//           disabled={!this.state.editable} // use true to disable edition
//           onChange={this.handleChange} // handle innerHTML change
//           onBlur={this.sanitize}
//         />
//         <h3>source</h3>
//         <textarea
//           className="editable"
//           value={this.state.html}
//           onChange={this.handleChange}
//           onBlur={this.sanitize}
//         />
//         <h3>actions</h3>
//         <EditButton cmd="italic" />
//         <EditButton cmd="bold" />
//         <EditButton cmd="formatBlock" arg="h1" name="heading" />
//         <EditButton
//           cmd="createLink"
//           arg="https://github.com/lovasoa/react-contenteditable"
//           name="hyperlink"
//         />
//         <EditButton
//           cmd="span"
//         />
//         <button onClick={this.toggleEditable}>
//           Make {this.state.editable ? "readonly" : "editable"}
//         </button>
//         {Tags.map((data) => {
//           if ('task' === data.tag_type) {
//             return <button key={data._id} type="button" id={data.type} onMouseDown={(e) => {
//               e.preventDefault();
//               const content = document.createElement("span");
//               content.setAttribute('class', 'span-tag');
//               content.setAttribute('id', data._id);
//               content.setAttribute('contenteditable', 'false');
//               content.innerText = data.type

//               const wait = '<span class="span-tag" contenteditable="false" id="' + data._id + '">' + data.type + '</span>'

//               document.execCommand("insertHTML", false, wait)
//             }}>{data.name}</button>
//           }
//         })}
//       </div>
//     );
//   };
// }

// function EditButton(props) {
//   return (
//     <button
//       key={props.cmd}
//       onMouseDown={(evt) => {
//         evt.preventDefault(); // Avoids loosing focus from the editable area
//         document.execCommand(props.cmd, false, props.arg); // Send the command to the browser
//         console.log(props.cmd, false, props.arg)
//       }}
//     >
//       {props.name || props.cmd}
//     </button>
//   );
// }

// const rootElement = document.getElementById("root");
// ReactDOM.render(<MyComponent />, rootElement);

