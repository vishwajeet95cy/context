import { createContext } from "react";
import io from 'socket.io-client';

const socket = io;

const authContext = createContext({
  authenticated: false,
  setAuthenticated: (auth) => { }
});

export default authContext;