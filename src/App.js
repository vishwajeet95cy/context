import logo from './logo.svg';
import './App.css';
import ContentEditable from './NewContentEdit.tsx';
import { Tags } from './Tags';

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      {Tags.map((data) => {
        if ('task' === data.tag_type) {
          return <button key={data._id} type="button" id={data.type} onMouseDown={(e) => {
            e.preventDefault();
          }}>{data.name}</button>
        }
      })}
      <ContentEditable html="<div>Welcome</div>"></ContentEditable>
    </div>
  );
}

export default App;
